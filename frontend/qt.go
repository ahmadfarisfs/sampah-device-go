package frontend

import be "sampah-device-go/daurbeproxy"

type QTBackEndEvent struct {
}

func (q QTBackEndEvent) UserComing(role be.UserRole) {
	//call function on qml
}
func (q QTBackEndEvent) ScanningChamber(state ScanningChamberState, sumOfTrash int) {
	//call function on qml
}
func (q QTBackEndEvent) ShowError(message string) {
	//call function on qml
}

type QTFrontEndEvent struct{}

func (q QTFrontEndEvent) SetEnableTrashInputCallback(function interface{}) {}
func (q QTFrontEndEvent) SetUserLogoutCallback(function interface{})       {}
