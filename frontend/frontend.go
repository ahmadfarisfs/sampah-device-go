package frontend

import (
	be "sampah-device-go/daurbeproxy"
)

type FrontEndPage int

const (
	AdminPage FrontEndPage = iota
	AdminTimeoutPage
	IdlePage
	WaitQRage
	TrashInputPage
	ValidatingPge
	InvalidUserPage
	SummaryPage
	SuccessPage
	CheckUserPage
	ErrorPage

	MachineFailurePage
	ServerTimeoutPage
)

type FrontEndSignal int

const (
	OkSignal            FrontEndSignal = 0
	CancelSignal        FrontEndSignal = 1
	EndTrashInputSignal FrontEndSignal = 2
)

type FrontEndConnectorFamily interface {
	ShowPage(FrontEndPage, string)
	WaitUserInput(timeoutS int, signal FrontEndSignal) error
}

type ScanningChamberState int

const (
	IncomingTrash ScanningChamberState = iota
	ScanningTrash
	TrashAccepted
	TrashRejected
)

type BackEndEvent interface {
	UserComing(role be.UserRole)
	ScanningChamber(state ScanningChamberState, sumOfTrash int)
	ShowError(message string)
}

type FrontEndEvent interface {
	SetEnableTrashInputCallback(function interface{})
	SetUserLogoutCallback(function interface{})
}
