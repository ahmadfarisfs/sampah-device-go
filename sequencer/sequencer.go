package sequencer

import (
	"log"
	"sampah-device-go/barcode"
	"sampah-device-go/daurbeproxy"
	"sampah-device-go/daurhw"
	fe "sampah-device-go/frontend"
)

type DaurSequencer struct {
	qrCodeReader barcode.BarcodeReaderFamily
	proxy        daurbeproxy.DaurProxyFamily
	chamber      daurhw.ScanningChamber
	frontEnd     fe.FrontEndConnectorFamily
}

func (d DaurSequencer) HandleError(err error) {
	if err.MachineFailure() {
		//Machine is broken, better call maintenance !
		d.frontEnd.ShowPage(fe.MachineFailurePage, err.Error())
		//tell backend that this machine is error, dont consume anymore error
		errs := d.proxy.FailureBoxReport(err.Error())
		log.Panicln(err)
		log.Panicln(errs)
		//Freeze action
		for {
		}
		//return false
	}
}

func (d DaurSequencer) Run() {
	for {
		d.frontEnd.ShowPage(fe.IdlePage, "")
		qrData := d.qrCodeReader.WaitForInput(-1)
		d.frontEnd.ShowPage(fe.CheckUserPage, "")
		userRole, sessionID, userID, err := d.proxy.CheckUser(qrData)
		d.HandleError(err)

		if userRole == daurbeproxy.DaurAdmin {
			//ADMIN MODE
			d.frontEnd.ShowPage(fe.AdminPage, "")
			err := d.frontEnd.WaitUserInput(500, fe.OkSignal)
			//If admin timeout, tell FE to display it first
			if err.UserTimeout {
				d.frontEnd.ShowPage(fe.AdminTimeoutPage, "")
				d.frontEnd.WaitUserInput(5, fe.OkSignal)
				//If admin time out, then back to wait for input
			}
			errConn := d.proxy.EndSession(sessionID)
			d.HandleError(errConn)
			continue

		} else if userRole == daurbeproxy.DaurUser {
			//USER MODE
			retries := 4 // maximum rejection for one session
			bottles := make(map[string]uint64)
			for {
				d.frontEnd.ShowPage(fe.TrashInputPage, "")
				err := d.chamber.Enable()
				d.HandleError(err)

				//var sku string
				inputTrashChan := make(chan struct{})
				userInputChan := make(chan struct{})
				go func() {
					//Wait for incoming things, asal ada yang kedetek sensor dulu
					err = d.chamber.WaitBottle(100)
					d.HandleInputError(err)
					close(inputTrashChan)
				}()

				go func() {
					errs := d.frontEnd.WaitUserInput(-1, daurhw.EndTrashInputSignal)
					d.HandleInputError(errs)
					close(userInputChan)
				}()

				select {
				case <-inputTrashChan:
					skuChan := make(chan string)
					go func() {
						sku, errBarcode := d.chamber.WaitBarcode(10)
						d.HandleInputError(errBarcode)
						skuChan <- sku
					}()

					select {
					case sku := <-skuChan:
						var validity bool
						validity, err = d.proxy.CheckSKU(sku)
						if err != nil {
							d.frontEnd.ShowPage(daurhw.ErrorPage, err.Error())
							break
						}
						if validity {
							d.chamber.AcceptBottle()
							//add to map
							if sum, ok := bottles[sku]; ok {
								bottles[sku] = sum + 1

							} else {
								bottles[sku] = 1
							}
						} else {
							d.chamber.RejectBottle()
							retries--
							if retries == 0 {
								//userban
								d.proxy.BanUser(userID, "Too many duude")
							} else {
								//show page masih boleh retries
							}
						}
						d.chamber.Disable()
					case <-userInputChan:

					}

				case <-userInputChan:
					break
				}

				//todo: If timeout or if done inputting trash is pressed

			}
			d.frontEnd.ShowPage(daurhw.SummaryPage, "")
			d.frontEnd.WaitUserInput(100, daurhw.OkSignal)
			d.proxy.TrashInputReport(bottles, userID, sessionID)
			d.proxy.EndSession(sessionID)
			d.frontEnd.ShowPage(daurhw.SuccessPage, "")
		} else {
			d.frontEnd.ShowPage(daurhw.InvalidUserPage, "")
			d.frontEnd.WaitUserInput(100, daurhw.OkSignal)
		}
		/*	elif qr_role == "USER":
			while(True):
				show_trash_input()
				status = wait_trash()
				if status == "TIMEOUT":
					show_timeout_notice()
					break
				elif status == "INPUTTRIGGER":
					is_valid = validate_trash()
					if is_valid:
						full = check_bin()
						if full:
							show_full_notice()
							break
					else:
						invalid_counter += 1
						if invalid_counter > 4:
							show_acc_suspended()
							wait_user_input()
							break
						show_invalid(invalid_counter)
						wait_user_input()
				elif status == "DONE":
					break
			success = checkout()
			if success:
				show_success_notice()
			else:
				show_payment_error()
			wait_user_input()*/
	}
}
