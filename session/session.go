package session

//Manager will held current state and shared globally, should be thread-safe
type Manager struct {
	UserID    string
	SessionID string
	Idle      bool
	Bottles   map[string]int
}

func (s *Manager) Register(userID string, sessionID string) {
	s.Idle = false
	s.SessionID = sessionID
	s.UserID = userID
	s.Bottles = make(map[string]int)
}

func (s *Manager) End() {
	s.Idle = true
	s.Bottles = nil
}

func (s Manager) IsIdle() bool {
	return s.Idle
}

func (s *Manager) AddBottles(sku string) {
	s.Bottles[sku]++
}

func (s Manager) GetBottles() map[string]int {
	return s.Bottles
}
