package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sampah-device-go/barcode"
	"sampah-device-go/daurhw"
	"sampah-device-go/frontend"
	"sampah-device-go/sampahduino"
	"sampah-device-go/session"
	"strings"
	"sync"

	"time"
)

func IsClosed(ch <-chan struct{}) bool {
	select {
	case <-ch:
		return true
	default:
	}

	return false
}

// bug list:
//1.unreiable channel comm, sometimes nyangkut
//2. SOmetimes serial comm corrupt
//3.

// waitTimeout waits for the waitgroup for the specified max timeout.
// Returns true if waiting timed out.
func waitTimeout(wg *sync.WaitGroup, timeout time.Duration) bool {
	c := make(chan struct{})
	go func() {
		defer close(c)
		wg.Wait()
	}()
	select {
	case <-c:
		return false // completed normally
	case <-time.After(timeout):
		return true // timed out
	}

}
func ErrorScanCallback(payload error) {
	fmt.Println(payload)
}

func waitInput() string {
	buf := bufio.NewReader(os.Stdin)
	//fmt.Print("> ")
	sentence, _, err := buf.ReadRune()
	if err != nil {
		panic("error reading input")
		//fmt.Println(err)
	} else {
		return strings.TrimSpace(string(sentence))
		//fmt.Println(string(sentence))
	}
}

func mavin() {

	sb := barcode.KeyboardQRScanner{
		Address: "/dev/input/event5",
	}
	sb.StartListening()
	for {
		str := sb.ReadData()
		if str != "" {
			log.Println("WOW: ", str)

		}
	}
	//usb0 57600 -> honeywell
	//usb1 9600 -> symbol

	//Dummy valid skus
	validSKU := []string{
		"8992761002015",
		"8996001600269",
		"8992761139018",
	}

	//Declare interface
	var brFam barcode.BarcodeReaderFamily
	var duinoFamily sampahduino.SampahDeviceFamily

	//Instansiate barcode sku reader
	//bs := barcode.VirtualBarcodeReader{DelayTimeS: 4}
	bs, errs := barcode.CreateSerialBarcode("/dev/ttyUSB0", 9600)
	if errs != nil {
		fmt.Println(errs)
	}
	bs.StartListening()

	//second serial barcode, symbol
	bs2 := barcode.VirtualBarcodeReader{DelayTimeS: 1000}
	//	bs2, errs2 := barcode.CreateSerialBarcode("/dev/ttyUSB1", 9600)
	//	if errs2 != nil {
	//		fmt.Println(errs2)
	//	}
	bs2.StartListening()
	//	go func() {
	//		bs2.StartListening()

	//Instantiate real duino
	//duino, err := sampahduino.CreateSampahDevice("/dev/ttyUSB2", 57600, 1)
	//if err != nil {
	//		fmt.Println(err)
	//	}
	duino := sampahduino.VirtualDuino{
		BoolStatus:  []bool{false, false, false},
		TofStatus:   []int{100, 100, 100},
		UltraStatus: []int{100, 100, 100},
	}

	//Assign to interface for flexibility
	duinoFamily = duino
	brFam = &bs
	listOfBarcode := []barcode.BarcodeReaderFamily{brFam}
	//Setting for servo arm
	servoL := daurhw.ServoBinaryValue{Id: 4, OpenValue: 130, CloseValue: 70} //rejector sens
	servoR := daurhw.ServoBinaryValue{Id: 2, OpenValue: 60, CloseValue: 110} //Rejector rot
	servoC := daurhw.ServoBinaryValue{Id: 1, OpenValue: 0, CloseValue: 100}  //Acceptor servo
	sc := daurhw.CreateScanningChamber(listOfBarcode, duinoFamily, servoC, servoR, servoL)

	//-- end of factory --//

	//Set channel for bottle detection
	tofCh := []int{2}
	ultraCh := []int{2}
	boolCh := []int{1}
	sc.SetPresenceSensorChannel(tofCh, ultraCh, boolCh)

	//Set threshold for detecting bottle
	sc.SetDetectRangeThreshold(70.0, 20.0)

	//Set config for bottle and barcode rotator
	sc.SetRotatorConfig(0, 0.0, 1.0, 70, 110)

	for {
		fmt.Println("Wait user input, press e to end, enter to accept trash")
		if waitInput() == "e" {
			break
		}
		fmt.Println("Please Insert Bottle")
		sc.Enable()
		sku := ""
		bottleDetected := false
		isTimeout := false
		var wg sync.WaitGroup
		wg.Add(1)
		timeStart := time.Now()
		go func() {
			for {
				bsSKU := bs.ReadData()
				if bsSKU != "" && sku == "" {
					log.Println("SKU Detected from bs1: ", bsSKU)
					sku = bsSKU
				}
				bs2SKU := bs2.ReadData()
				if bs2SKU != "" && sku == "" {
					log.Println("SKU Detected from bs2: ", bs2SKU)
					sku = bs2SKU
				}
				isBottle := sc.IsBottleDetected()
				if isBottle == true && bottleDetected == false {
					log.Println("Bottle detected")
					bottleDetected = true
				}
				if bottleDetected && sku != "" {
					//complete
					wg.Done()
					break
				}
				if time.Now().After(timeStart.Add(time.Duration(20) * time.Second)) {
					//timeout
					wg.Done()
					isTimeout = true
					break
				}
			}
		}()
		wg.Wait()

		if isTimeout {
			log.Println("Cannot read trash, timeput")
			sc.RejectBottle()
		} else {
			if (sku != "" && bottleDetected) == true {
				if contains(validSKU, sku) {
					log.Println("Valid Trash")
					sc.AcceptBottle()
				} else {
					log.Println("Invalid Trash")
					sc.RejectBottle()
				}
			} else {
				log.Println("should not be here, timeout should cover this state")
				panic("illegal state")
			}
		}
		sc.Disable()
	}
	//---------------------------------------------------------new session
	for {
		fmt.Println("Wait user input, press e to end, enter to accept trash")

		if waitInput() == "e" {
			break
		}

		fmt.Println("Please Insert Bottle")
		//Enable reading
		sc.Enable()
		//session variable
		isBarcodeDetected := false
		isBottlePlaced := false
		isTimeout := false
		var wgBarcode sync.WaitGroup
		var wgBottle sync.WaitGroup
		terminateBarcodeChan := make(chan struct{})
		terminateBottleChan := make(chan struct{})
		sku := ""

		wgBarcode.Add(1) //should always be 1, does not matter how many barcode
		go func() {
		Loop:
			for {
				strScan := bs.ReadData()
				if strScan != "" {
					sku = strScan
					log.Println("Barcode from device 1 detected: " + strScan)
					isBarcodeDetected = true
					wgBarcode.Done()
					//close(terminateBarcodeChan)
					break Loop
				}
				select {
				case <-terminateBarcodeChan:
					log.Println("Barcode device 1 terminated")
					break Loop
				default:
					time.Sleep(time.Duration(500) * time.Millisecond)
				}
			}
		}()

		wgBottle.Add(1)
		go func() {
		Loop:
			for {
				if sc.IsBottleDetected() {
					log.Println("Bottle detected")
					isBottlePlaced = true
					wgBottle.Done()
					//close(terminateBottleChan)
					break Loop
				}
				select {
				case <-terminateBottleChan:
					log.Println("Bottle waiting terminated")
					break Loop
				default:
					time.Sleep(time.Duration(500) * time.Millisecond)
				}
			}
		}()

		var wgAll sync.WaitGroup
		wgAll.Add(2) // waiting barcode and bottle
		go func() {
			if waitTimeout(&wgBarcode, time.Duration(20)*time.Second) {
				log.Println("Waiting Barcode: Timeout")
				isTimeout = true
			} else {
				log.Println("Watiting Barcode: succes")
			}
			close(terminateBarcodeChan) //close all goroutine handling reading barcode
			wgAll.Done()
		}()

		go func() {
			if waitTimeout(&wgBottle, time.Duration(20)*time.Second) {
				log.Println("Waiting Bottle: Timeout")
				isTimeout = true
			} else {
				log.Println("Watiting Bottle: succes")
			}
			close(terminateBottleChan) //close all goroutine handling reading bottle presence
			wgAll.Done()
		}()

		wgAll.Wait() //this wait should always exit

		if isTimeout {
			log.Println("Cannot read trash")
			sc.RejectBottle()
		} else {
			if (isBarcodeDetected && isBottlePlaced) == true {
				if contains(validSKU, sku) {
					log.Println("Valid Trash")
					sc.AcceptBottle()
				} else {
					log.Println("Invalid Trash")
					sc.RejectBottle()
				}
			} else {
				log.Println("should not be here, timeout should cover this state")
				panic("illegal state")
			}
		}
		sc.Disable()
	}
	log.Println("end!")
	for {
	}
	/*	for {
			waitInput()
			fmt.Println("Enabling")
			sc.Enable()
			waitInput()
			fmt.Println("Accept Bottle")
			sc.AcceptBottle()
			waitInput()
			fmt.Println("Reject Bottle")
			sc.RejectBottle()
			waitInput()
			fmt.Println("Disable")
			sc.Disable()
		}
	*/
	for {
		//session variable
		isTimeout := false
		sku := ""
		detected := false

		fmt.Println("Wait user input")
		waitInput()
		fmt.Println("Please Insert Bottle")

		//Enable reading
		sc.Enable()

		skuChan := make(chan string)
		skuChan2 := make(chan string)

		//	skuFinalChan := make(chan string)
		//	bottleDetChan := make(chan bool)
		//	timeStart := time.Now()

		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			go func() {
				//	bs.StartListening()
				bssku := "" //bs.WaitData(-1)
				fmt.Println("BS1 SKU: ", bssku)
				skuChan <- bssku
				//defer close(skuChan)
			}()

			go func() {
				//	bs2.StartListening()
				bs2sku := "" //bs2.WaitData(-1)
				fmt.Println("BS2 SKU: ", bs2sku)

				skuChan2 <- bs2sku
				//defer close(skuChan2)
			}()

			var skuLoc string

			select {
			case sku2 := <-skuChan2:
				skuLoc = sku2
			case skus := <-skuChan:
				skuLoc = skus
			}

			if sku == "" {
				fmt.Println("SKU Scanned: ", sku)
				sku = skuLoc
				wg.Done()
			}
			//close(skuChan)
			//close(skuChan2)
		}()

		wg.Add(1)
		go func() {
			//	sc.RunBottleDetection()
			if !detected {
				detected = true
				fmt.Println("Bottle detected")
				wg.Done()
			}
		}()

		//wg.Wait()
		if waitTimeout(&wg, time.Second*20) {
			fmt.Println("Timed out waiting for wait group")
			isTimeout = true
		} else {
			fmt.Println("Wait group finished")
		}
		//		fmt.Println("Complete !")
		/*	for i := 0; i < 2; i++ {
				select {
				case skuC, ok := <-skuChan:
					if !ok {
						log.Println("SKU chan closed")
					} else {
						sku = skuC
						log.Println("SKU detected " + sku)
						//close chan, so only detect one barcode only if multiple reader present
						close(skuChan)
					}
				case val, ok := <-bottleDetChan:
					if !ok {
						log.Println("BOttlechan closed")
					} else {
						if val {
							log.Println("bottle Detected")
							detected = true
							close(bottleDetChan)
						} else {
							log.Println("bottle not Detected")
						}
					}
				case <-time.After(time.Duration(20) * time.Second):
					log.Println("reading timeout!")
					isTimeout = true
					close(skuChan)
					close(bottleDetChan)
					break
				}

			}
		*/

		if isTimeout {
			log.Print("Rejected because device  ")
			if !detected {
				log.Println("cannot detect bottle")
			}
			if sku == "" {
				log.Println(" cannot read sku")
			}
			log.Println(" until timeout")

			sc.RejectBottle()

		} else {
			if contains(validSKU, sku) {
				log.Println("Valid Trash")
				sc.AcceptBottle()
			} else {
				log.Println("Invalid Trash")
				sc.RejectBottle()
			}
		}
		sc.Disable()

	}
}

func maign() {

	simulationMode := true
	var duino sampahduino.SampahDeviceFamily
	var err error
	if simulationMode {

		duino = sampahduino.VirtualDuino{
			BoolStatus:  []bool{true, false, false},
			TofStatus:   []int{100, 100, 100},
			UltraStatus: []int{200, 100, 20},
		}

	} else {
		duino, err = sampahduino.CreateSampahDevice("/dev/cu.usbserial-1410", 57600, 5)
		if err != nil {
			log.Fatalln(err)
			return
		}
	}

	feHandler := frontend.QTFrontEndEvent{}

	//	beHandler := frontend.QTBackEndEvent{}

	sm := session.Manager{}

	//	scanningChamber := daurhw.CreateScanningChamber()
	//----------------end of factory

	feHandler.SetEnableTrashInputCallback(func() {

	})

	feHandler.SetUserLogoutCallback(func() {
		sm.End()
	})
	/*
		proxy := daurbeproxy.DaurProxy{}

			//service 1, create listener
			qrCodeChan := make(chan string)
			go barcode.ScanForever("/dev/input/event/0", qrCodeChan, ErrorScanCallback)
			go func() {
				// consumer of scan, here we are tried to validate what qr sees
				strQr := <-qrCodeChan
				if sm.IsIdle() {
					log.Println("QR Scanned !")
					userRole, sessionID, userID, err := proxy.CheckUser(strQr)
					if err != nil {
						log.Println(err)
						beHandler.ShowError(err.Error())
					} else {
						beHandler.UserComing(userRole)
						sm.Register(userID, sessionID)
						//open scanningchamber
					}
				} else {
					log.Println("QR Scanned, but still busy")
				}
			}()

			for {

			}
	*/
	log.Println("Sampah duino created")
	err = duino.Ping()
	if err != nil {
		log.Fatalln(err)
		return
	}
	log.Println("Getting bool")

	value, errb := duino.GetBool(0)
	if errb != nil {
		log.Fatalln(errb)
		return
	}
	log.Println(value)

	log.Println("Getting Ultrasonic")

	valueu, erru := duino.GetUltrasonic(0)
	if erru != nil {
		log.Fatalln(erru)
		return
	}
	log.Println(valueu)

	duino.StartReaderRotator()

	//tutup semua, idle
	duino.SetServo(1, 90)
	duino.SetServo(2, 110)
	duino.SetServo(4, 70)
	time.Sleep(time.Duration(2) * time.Second)

	//reject
	duino.SetServo(1, 90)
	duino.SetServo(2, 60)
	duino.SetServo(4, 130)
	time.Sleep(time.Duration(2) * time.Second)

	//accept
	duino.SetServo(1, 0)
	duino.SetServo(2, 110)
	duino.SetServo(4, 70)
	time.Sleep(time.Duration(2) * time.Second)
	duino.StopReaderRotator()

	time.Sleep(time.Duration(2) * time.Second)

	for i := 0; i < 4; i++ {
		val, err := duino.GetBool(i)
		if err != nil {
			panic("Error reading bool")
		}
		log.Printf("Bool %d val: %t\r\n", i, val)
	}

	for i := 0; i < 3; i++ {
		val, err := duino.GetTof(i)
		if err != nil {
			panic("Error reading tof")
		}
		log.Printf("Tof %d val: %d\r\n", i, val)
	}

}

/*
func main() {
	qrReader := qrcode.FindBarcodeDevice()
	if qrReader == "" {
		log.Fatalln("Cannot find barcode device")
	}
	qrChannel := make(chan []byte)
	go qrcode.QRCodeReaderWait(qrChannel)

	for {
		qrCodeString := string(<-qrChannel)

	}
}


   qr_data = wait_qr_input()
   qr_role = check_qr_role(qr_data)
   if qr_role == "ADMIN":
       show_admin()
       wait_user_input()
       continue
   elif qr_role == "USER":
       while(True):
           show_trash_input()
           status = wait_trash()
           if status == "TIMEOUT":
               show_timeout_notice()
               break
           elif status == "INPUTTRIGGER":
               is_valid = validate_trash()
               if is_valid:
                   full = check_bin()
                   if full:
                       show_full_notice()
                       break
               else:
                   invalid_counter += 1
                   if invalid_counter > 4:
                       show_acc_suspended()
                       wait_user_input()
                       break
                   show_invalid(invalid_counter)
                   wait_user_input()
           elif status == "DONE":
               break
       success = checkout()
       if success:
           show_success_notice()
       else:
           show_payment_error()
       wait_user_input()
*/
