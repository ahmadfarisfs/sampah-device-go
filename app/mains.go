package main

import (
	"bufio"
	"log"
	"os"
	"sampah-device-go/barcode"
	"sampah-device-go/daurbeproxy"
	"sampah-device-go/daurhw"
	"sampah-device-go/sampahduino"
	"sampah-device-go/validator"
	"strings"
	"time"

	"github.com/joho/godotenv"
)

func waitInput() string {
	buf := bufio.NewReader(os.Stdin)
	//fmt.Print("> ")
	sentence, _, err := buf.ReadRune()
	if err != nil {
		panic("error reading input")
		//fmt.Println(err)
	} else {
		return strings.TrimSpace(string(sentence))
		//fmt.Println(string(sentence))
	}
}

func waitInputAfter() <-chan string {
	r := make(chan string)

	go func() {
		defer close(r)
		r <- waitInput()
	}()

	return r
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func parseLines(filePath string, parse func(string) (string, bool)) ([]string, error) {
	inputFile, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)
	var results []string
	for scanner.Scan() {
		if output, add := parse(scanner.Text()); add {
			results = append(results, output)
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return results, nil
}

func waitUserScan(reader barcode.BarcodeReaderFamily) string {
	for {
		qrSecret := reader.ReadData()
		if qrSecret != "" {
			return qrSecret
		}
		time.Sleep(time.Duration(500) * time.Millisecond)
	}
}

func main() {
	validSKU := []string{
		"8992761002015",
		"8996001600269",
		"8992761139018",
		"8992761122317",
		"8992761164539",
		"8992761147143",
	}
	file, err := os.Open("whitelist_bottle.txt")

	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		validSKU = append(validSKU, scanner.Text())
	}

	file.Close()

	err = godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	simulated := false
	var userQrReader barcode.BarcodeReaderFamily
	var skuScannerA barcode.BarcodeReaderFamily
	var skuScannerB barcode.BarcodeReaderFamily
	var scanningChamber daurhw.ScanningChamber
	var duinoDevice sampahduino.SampahDeviceFamily
	var proxy daurbeproxy.DaurProxyFamily

	if !simulated {
		//QR Scanner for scanning user's screen
		sunmiQR := barcode.KeyboardQRScanner{
			Address: os.Getenv("QR_SCANNER_ADDRESS"),
		}
		symbolScanner := barcode.KeyboardQRScanner{Address: os.Getenv("BC_SCANNER_ADDRESS1")}
		quantumScanner := barcode.KeyboardQRScanner{Address: os.Getenv("BC_SCANNER_ADDRESS2")}
		duino, err := sampahduino.CreateSampahDevice(os.Getenv("CONTROLLER_ADDRESS"), 57600, 1)
		if err != nil {
			panic("Cannot create duino device, " + err.Error())
		}

		skuScannerA = &quantumScanner
		skuScannerB = &symbolScanner
		userQrReader = &sunmiQR
		duinoDevice = duino

	} else {
		skuScannerA = &barcode.VirtualBarcodeReader{Data: ""}
		skuScannerB = &barcode.VirtualBarcodeReader{Data: ""}
		userQrReader = &barcode.VirtualBarcodeReader{Data: "CKRaHQlt10g1YjZjN2I1My1mYjc1LTRjOTgtYWI5OC0yYTQzOGUzZTQ1ZTk=", DelayTimeS: 5, Created: time.Now()}
		duinoDevice = sampahduino.VirtualDuino{
			BoolStatus:  []bool{true, false, false},
			TofStatus:   []int{100, 100, 100},
			UltraStatus: []int{200, 100, 20},
		}
	}

	servoL := daurhw.ServoBinaryValue{Id: 4, OpenValue: 130, CloseValue: 70} //rejector sens
	servoR := daurhw.ServoBinaryValue{Id: 2, OpenValue: 60, CloseValue: 110} //Rejector rot
	servoC := daurhw.ServoBinaryValue{Id: 1, OpenValue: 0, CloseValue: 100}  //Acceptor servo

	scanningChamber = daurhw.CreateScanningChamber(
		[]barcode.BarcodeReaderFamily{skuScannerA, skuScannerB},
		duinoDevice,
		servoC,
		servoR,
		servoL,
	)
	//Set channel for bottle detection
	tofCh := []int{2}
	ultraCh := []int{2}
	boolCh := []int{1}
	listOfSKUScanner := []barcode.BarcodeReaderFamily{skuScannerA, skuScannerB}
	scanningChamber.SetPresenceSensorChannel(tofCh, ultraCh, boolCh)

	//Set threshold for detecting bottle
	scanningChamber.SetDetectRangeThreshold(70.0, 20.0)

	//Set config for bottle and barcode rotator
	scanningChamber.SetRotatorConfig(0, 0.0, 1.0, 70, 110)

	//set shutter config
	scanningChamber.SetShutterConfig(-270, 270)

	prx := daurbeproxy.CreateDaurProxy("dev-api.omitsindo.com", 5)
	proxy = &prx

	gValidator := validator.GVision{JSONLocation: "silmioti.json"}

	skuScannerA.StartListening()
	skuScannerB.StartListening()

	//---------------------------------------------------------------------//
	// Begin sync
	userQrReader.StartListening()
	//	proxy.RegisterBox("MACTEST01", "HOSTNAMETEST01")
	proxy.LoginBox("MACTEST01", "HOSTNAMETEST01")
	//start running
	userQrReader.WaitQR(func(qrSecret string) {
		log.Println("Scanned")
		userRole, sessionID, userID, errCheck := proxy.CheckUser(qrSecret)
		log.Println(sessionID + " " + userID + " ")
		if errCheck != nil {
			log.Println("Error Login: BUT WE WILL CONTINUE FOR NOW " + errCheck.Error())
			time.Sleep(1 * time.Second)
			return
		}
		scanningChamber.Enable()

		userRole = daurbeproxy.DaurUser

		log.Println("Scanned:" + qrSecret)
		if userRole == daurbeproxy.DaurUser {
			//hey it's you
			log.Println("User Scanned: " + userID)
			log.Println("Please Input trash")

			trashList := map[string]uint64{}
			userCancel := make(chan struct{})

		TrashInput:
			for {
				sku := ""
				bottleDetected := false
				canceled := false
				trashInput := make(chan struct{})

				go func() {
					for {
						if scanningChamber.IsBottleDetected() {
							bottleDetected = true
							close(trashInput)
							log.Println("Scanning Chamber detected object")
							break
						}
						if canceled {
							log.Println("Scanning Chamber detection stopped")
							break
						}
						time.Sleep(500 * time.Millisecond)
					}
				}()

				for _, scanner := range listOfSKUScanner {
					scanner.IgnoreScan(false)
					go func(reader barcode.BarcodeReaderFamily) {
						for {
							strSku := reader.ReadData()
							if strSku != "" {
								log.Println("Scanner " + reader.Name() + " sku det: " + strSku)
								sku = strSku
							}

							if sku != "" {
								log.Println("Scanner " + reader.Name() + " stopped. sku detected")
								reader.IgnoreScan(true)
								break
							}

							if bottleDetected {
								log.Println("Scanner " + reader.Name() + " stopped. bottle detected")
								reader.IgnoreScan(true)
								break
							}

							if canceled {
								log.Println("Scanner " + reader.Name() + " stopped. user cancel")
								break
							}

							time.Sleep(100 * time.Millisecond)
						}
					}(scanner)
				}

				select {
				case <-time.After(time.Duration(20) * time.Second):
					canceled = true
					log.Println("Timeout entry")
					break TrashInput
				case <-trashInput:
					scanningChamber.CloseShutter()
					if sku == "" {
						//if sku not detected, we use vision
						log.Println("Using vision, because SKU is not detected")
						pred, err := gValidator.Recognize("/tmp/temp.jpg")
						if err != nil {
							log.Println("Cannot Use Vision: " + err.Error())
							scanningChamber.RejectBottle()
						} else {
							isBottle, detectedMaterial := validator.Validate(pred)
							if isBottle {
								switch detectedMaterial {
								case validator.PlasticMaterial:
									log.Println("Accepted Plastic Bottle")
									trashList["plastic_bottle"]++
								case validator.GlassMaterial:
									log.Println("Accepted Glass Bottle")
									trashList["glass_bottle"]++
								default:
									log.Println("Accepted Unknown Bottle")
									trashList["bottle"]++
								}
								scanningChamber.AcceptBottle()
							} else {
								log.Println("Rejected Because vision says so")
								scanningChamber.RejectBottle()
							}
						}
					} else {
						//sku detected and bottle is presence
						log.Println("Using Barcode detection method")
						if contains(validSKU, sku) {
							log.Println("Valid Trash Based on SKU")
							trashList[sku]++
							scanningChamber.AcceptBottle()
						} else {
							log.Println("Invalid Trash based on SKU")
							scanningChamber.RejectBottle()
						}
					}
					scanningChamber.OpenShutter()
				case <-userCancel:
					canceled = true
					log.Println("User Cancel")
					break TrashInput
				}
			}

			log.Println("Reporting to backend")
			errTrashInput := proxy.TrashInputReport(trashList, userID, sessionID)
			if errTrashInput != nil {
				log.Println(errTrashInput)
			}

			log.Println("Ending session")
			errLogout := proxy.EndSession(sessionID)
			if errLogout != nil {
				log.Println(errLogout)
			}
			log.Println("Session Done!")

		} else {
			log.Println("Invalid QR")
		}
		scanningChamber.Disable()

	})
}
