package main

import (
	"log"
	v "sampah-device-go/validator"
)

func main() {
	cl := v.GVision{
		JSONLocation: "silmioti.json",
	}
	cl.CaptureImage()
	maps, err := cl.Recognize()
	if err != nil {
		log.Println(err)
		panic(err)
	}
	log.Printf("%v", maps)

}
