package main

import (
	"log"
	"sampah-device-go/barcode"
)

func main() {
	kb1 := barcode.KeyboardQRScanner{
		Address: "/dev/input/event21",
	}
	kb2 := barcode.KeyboardQRScanner{
		Address: "/dev/input/event26",
	}

	kb1.StartListening()
	kb2.StartListening()
	log.Println("TEST")

	for {
		kb1sku := kb1.ReadData()
		kb2sku := kb2.ReadData()
		if kb1sku != "" {
			log.Println("KB1 " + kb1sku)
		}
		if kb2sku != "" {
			log.Println("KB2 " + kb2sku)
		}
	}
}
8992761164539
8992761164539
