package main

import (
	"log"
	"sampah-device-go/barcode"
	"time"
)

func main() {
	symbolScanner, err := barcode.CreateSerialBarcode("/dev/ttyUSB0", 57600)
	if err != nil {
		panic("Cannot create barcode scanner, " + err.Error())
	}

	symbolScanner.StartListening()
	for {``
		strSku := symbolScanner.ReadData()
		if strSku != "" {
			log.Println("Scanner " + symbolScanner.Name() + " sku det: " + strSku)
			//sku = strSku
		}

		time.Sleep(200 * time.Millisecond)
	}
}
