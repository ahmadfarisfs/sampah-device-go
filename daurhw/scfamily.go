package daurhw

type ScanningChamberInterface interface {
	Enable() error
	Disable() error
	ObjectInsertedEvent()
	SKURecognizedEvent()

	Accept() error
	Reject() error
}
