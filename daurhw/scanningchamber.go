package daurhw

import (
	"errors"
	"log"
	"sampah-device-go/barcode"
	"strconv"
	"sync"
	"time"

	//	daur "sampah-device-go/error"
	"sampah-device-go/sampahduino"
)

type ScanningChamberState int

const (
	Undefined     ScanningChamberState = -1
	Idle          ScanningChamberState = 0
	Scanning      ScanningChamberState = 1
	WaitForAction ScanningChamberState = 2
	Error         ScanningChamberState = 99
)

type ServoBinaryValue struct {
	OpenValue  uint
	CloseValue uint
	Id         int
}

type ScanningChamber struct {
	//CurrentState ScanningChamberState

	device sampahduino.SampahDeviceFamily

	barcodeDevice []barcode.BarcodeReaderFamily
	//	barcodePayload      chan string
	barcodeRotatorSpeed float32

	bottleRotatorMotorChannel int
	bottleRotatorSpeed        float32

	tofChannel   []int
	tofThreshold int

	ultrasonicChannel   []int
	ultrasonicThreshold int

	boolChannel []int

	//	scanTimeoutS int
	acceptorConfig     ServoBinaryValue
	rejectorSensConfig ServoBinaryValue
	rejectorRotConfig  ServoBinaryValue

	stepperOpenAngle  int
	stepperCloseAngle int

	validatorCB func(string) bool
}

func CreateScanningChamber(barcodeReader []barcode.BarcodeReaderFamily, sampahDuino sampahduino.SampahDeviceFamily, acceptorConfig ServoBinaryValue, rejectorRotConfig ServoBinaryValue, rejectorSensConfig ServoBinaryValue) ScanningChamber {
	scanningChamber := ScanningChamber{
		device:        sampahDuino,
		barcodeDevice: barcodeReader,
		//	barcodePayload:      make(chan string),
		barcodeRotatorSpeed: 1.0,
		rejectorRotConfig:   rejectorRotConfig,
		rejectorSensConfig:  rejectorSensConfig,
		acceptorConfig:      acceptorConfig,
	}

	return scanningChamber
}

func (s *ScanningChamber) SetValidatorCallback(cb func(string) bool) {
	s.validatorCB = cb
}

func (s *ScanningChamber) SetRotatorConfig(bottleRotatorMotorChannel int, bottleSpeed float32, barcodeSpeed float32, barcodeStartAngle uint8, barcodeStopAngle uint8) {
	if bottleSpeed > 1.0 || bottleSpeed < -1.0 {
		log.Println("Bottle speed value outside valid range")
		return
	}
	if barcodeSpeed > 1.0 || barcodeSpeed < 0.0 {
		log.Println("Barcode speed value outside valid range")
		return
	}
	s.bottleRotatorMotorChannel = bottleRotatorMotorChannel
	s.bottleRotatorSpeed = bottleSpeed
	//s.barcodeRotatorSpeed = barcodeSpeed
	delay := uint8((1.0 - barcodeSpeed) * 255.0)
	s.device.SetReaderRotatorConfig(delay, barcodeStartAngle, barcodeStopAngle)
}

func (s *ScanningChamber) SetShutterConfig(openAngle int, closeAngle int) {
	s.stepperOpenAngle = openAngle
	s.stepperCloseAngle = closeAngle
}

//Enable will
func (s ScanningChamber) Enable() error {
	//s.CurrentState = Scanning

	// Open shutter
	s.OpenShutter()

	// Turn on bottle rotator, a hacked servo
	err := s.device.SetServo(s.bottleRotatorMotorChannel, uint(s.bottleRotatorSpeed*180))
	if err != nil {
		return err
	}

	// Turn on barcode rotator
	err = s.device.StartReaderRotator()
	if err != nil {
		return err
	}
	//	for _, dev := range s.barcodeDevice {
	//		dev.StartListening()
	//	}

	s.setIdle()
	return nil
}

func (s ScanningChamber) RejectBottle() {
	s.setReject()
	time.Sleep(1 * time.Second)
	s.setIdle()
}

func (s ScanningChamber) AcceptBottle() {
	s.setAccept()
	time.Sleep(3 * time.Second)
	s.setIdle()
}

func (s ScanningChamber) OpenShutter() {
	s.device.SetStepper(s.stepperOpenAngle)
}

func (s ScanningChamber) CloseShutter() {
	s.device.SetStepper(s.stepperCloseAngle)
}

func (s ScanningChamber) setIdle() {
	s.device.SetServo(s.acceptorConfig.Id, s.acceptorConfig.CloseValue)
	s.device.SetServo(s.rejectorRotConfig.Id, s.rejectorRotConfig.CloseValue)
	s.device.SetServo(s.rejectorSensConfig.Id, s.rejectorSensConfig.CloseValue)
}

func (s ScanningChamber) setAccept() {
	s.device.SetServo(s.acceptorConfig.Id, s.acceptorConfig.OpenValue)
	s.device.SetServo(s.rejectorRotConfig.Id, s.rejectorRotConfig.CloseValue)
	s.device.SetServo(s.rejectorSensConfig.Id, s.rejectorSensConfig.CloseValue)
}

func (s ScanningChamber) setReject() {
	s.device.SetServo(s.acceptorConfig.Id, s.acceptorConfig.CloseValue)
	s.device.SetServo(s.rejectorRotConfig.Id, s.rejectorRotConfig.OpenValue)
	s.device.SetServo(s.rejectorSensConfig.Id, s.rejectorSensConfig.OpenValue)
}

func (s ScanningChamber) Disable() error {
	//Close Shutter
	s.CloseShutter()

	// Turn off bottle rotator, a hacked servo
	err := s.device.SetServo(s.bottleRotatorMotorChannel, 90)
	if err != nil {
		return err
	}

	// Turn on barcode rotator
	err = s.device.StopReaderRotator()
	if err != nil {
		return err
	}
	//	for _, dev := range s.barcodeDevice {
	//		dev.StopListening()
	//	}
	//s.barcodeDevice.StopListening()
	//Reject Everything
	//	s.setReject()

	return nil
}

/*
func (s ScanningChamber) RunBarcodeDetection(sku chan string) {
	//	for {
	select {
	case sku <- s.barcodeDevice.WaitData(-1):
		log.Println("Detected barcode from bs scanning chamber")
		return
	case _, ok := <-sku:
		if !ok {
			log.Println("Run Barcode detection terminated")
		} else {
			log.Println("Run Barcode detection terminated but ok")

		}
		return
	}
	//	}
}*/

func (s ScanningChamber) IsBottleDetected() bool {
	isUltra, err := s.getUltrasonic()
	if err != nil {
		log.Fatalf("Cannot get Ultrasonic %v", err)
	}

	//	isTof, errTof := s.getTof()
	//	if errTof != nil {
	//		log.Fatalf("Cannot get Tof %v", err)
	//	}

	//	isBool, err := s.getBool()
	//	if err != nil {
	//		log.Fatalf("Cannot get Boolean sensor %v", err)
	//	}
	if isUltra { //} && isTof && isBool {
		//Bottle Detected
		log.Println("Bottle Detected")
		//	detected <- true
		return true
	}
	return false
}

//RunAndWait will enable scanning chamber, then wait for bottle to come, then calling validator callback
func (s ScanningChamber) RunAndWait(waitSKU bool, timeout time.Duration) (string, error) {
	s.Enable()
	sku := ""
	bottleDetected := false
	isTimeout := false
	var wg sync.WaitGroup
	wg.Add(1)
	timeStart := time.Now()
	go func() {
		for {
			for idx, bs := range s.barcodeDevice {
				bsSKU := bs.ReadData()
				if bsSKU != "" && sku == "" {
					log.Println("SKU Detected from bs" + strconv.Itoa(idx) + ": " + bsSKU)
					sku = bsSKU
				}
			}

			isBottle := s.IsBottleDetected()
			if isBottle == true && bottleDetected == false {
				log.Println("Bottle detected")
				bottleDetected = true
			}
			if waitSKU {
				if bottleDetected && sku != "" {
					//complete
					wg.Done()
					break
				}
			} else {
				if bottleDetected {
					wg.Done()
					break
				}
			}
			if time.Now().After(timeStart.Add(timeout)) {
				//timeout
				wg.Done()
				isTimeout = true
				break
			}
			time.Sleep(time.Duration(500) * time.Millisecond)
		}
	}()
	wg.Wait()

	if isTimeout {
		log.Println("Cannot read trash, timeout")
		s.RejectBottle()
		s.Disable()
		return sku, errors.New("Scanning Timeout")
	}

	//	if (sku != "" && bottleDetected) == true {
	/*	if s.validatorCB(sku) {
			log.Println("Accepting Trash")
			s.AcceptBottle()
		} else {
			log.Println("Rejecting Trash")
			s.RejectBottle()
		}*/
	//	}

	s.Disable()
	return sku, nil
}

func (s ScanningChamber) WaitBottle(detected chan struct{}, cancel chan struct{}) {
WaitLoop:
	for {
		if s.IsBottleDetected() {
			close(detected)
		}
		select {
		case <-cancel:
			break WaitLoop
		case <-detected:
			break WaitLoop
		default:
		}
	}
	//		log.Println("Ultra: ", isUltra, " Tof: ", isTof, " Bool: ", isBool)
	/*
			select {
			//		case detected <- detState:
			case _, ok := <-detected:
				//if channel is closed
				if !ok {
					//				log.Println("Bottle Detection terminated")
					return
				} else {
					//				log.Println("Bottle Detection terminated but ok")
					return

				}
			default:
				if isUltra && isTof && isBool {
					//Bottle Detected
					log.Println("Bottle Detected")
					detected <- true
					break
				}

			}
		}
		//	}
		//	}
		//}()
	*/
}

func (s *ScanningChamber) SetPresenceSensorChannel(tofCh []int, ultraCh []int, boolCh []int) {
	s.tofChannel = tofCh
	s.ultrasonicChannel = ultraCh
	s.boolChannel = boolCh
}

func (s *ScanningChamber) SetDetectRangeThreshold(ultrasonicThresMm uint, tofThresMm uint) {
	s.ultrasonicThreshold = int(ultrasonicThresMm)
	s.tofThreshold = int(tofThresMm)
}

func (s ScanningChamber) getUltrasonic() (bool, error) {
	ultraArr := []int{}
	//	log.Println("Get all ultrasonic with threshold ", s.ultrasonicThreshold, " mm")
	for _, id := range s.ultrasonicChannel {
		dist, err := s.device.GetUltrasonic(id)
		ultraArr = append(ultraArr, dist)
		if err != nil {
			//			log.Println("Error Getting Ultrasonic")
			return false, err
		}
		//		log.Println("Ultrasonic ID: ", id, " value: ", dist)
	}

	for _, val := range ultraArr {
		if val > s.ultrasonicThreshold {
			return false, nil
		}
	}

	return true, nil

}

func (s ScanningChamber) getTof() (bool, error) {
	tofArr := []int{}
	//	log.Println("Get all tof with threshold ", s.tofThreshold, " mm")
	for _, id := range s.tofChannel {
		dist, err := s.device.GetTof(id)
		tofArr = append(tofArr, dist)
		if err != nil {
			//			log.Println("Error Getting Tof")
			err := errors.New("Error Getting Tof")

			return false, err
		}
		//		log.Println("Tof ID: ", id, " value: ", dist)
	}

	for _, val := range tofArr {
		if val > s.tofThreshold {
			//			log.Println("Get tof success, false")
			return false, nil
		}
	}
	//	log.Println("Get tof success, all true")
	return true, nil
}

func (s ScanningChamber) getBool() (bool, error) {
	boolArr := []bool{}
	//	log.Println("Get all bool sensor")
	for _, id := range s.boolChannel {
		val, err := s.device.GetBool(id)
		boolArr = append(boolArr, val)
		if err != nil {
			//			log.Println("Error Getting Bool")
			err := errors.New("Error Getting Bool ")

			return false, err
		}
		//		log.Println("Bool ID: ", id, " value: ", val)
	}

	for _, val := range boolArr {
		if val == false {
			//			log.Println("Get bool success, false")
			return false, nil
		}
	}
	//	log.Println("Get bool success, all true")
	return true, nil
}
