package barcode

import (
	"log"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/meriororen/keylogger"
)

func IsLetter(test string) bool {
	IsLetter := regexp.MustCompile(`^[a-zA-Z]*$`).MatchString
	return IsLetter(test)
}

func SymbolToUpper(symbol rune) rune {
	maps := map[rune]rune{
		'1':  '!',
		'2':  '@',
		'3':  '#',
		'4':  '$',
		'5':  '%',
		'6':  '^',
		'7':  '&',
		'8':  '*',
		'9':  '(',
		'0':  ')',
		'-':  '_',
		'=':  '+',
		'[':  '{',
		']':  '}',
		'\\': '|',
		'`':  '~',
		'\'': '"',
		',':  '<',
		'.':  '>',
		'/':  '?',
		';':  ':',
	}

	return maps[symbol]
}

func (ks KeyboardQRScanner) qrCodeReaderWait(device string, out chan []byte) {
	k, err := keylogger.New(device)
	if err != nil {
		log.Print(err)
		return
	}
	//defer k.Close()

	ch := make([]byte, 1024)
	upper := false
	events := k.Read()
	i := 0

Loop:
	for {
		select {
		case <-ks.cancel:
			//close(events)
			break Loop
		case e := <-events:
			switch e.Type {
			case keylogger.EvKey:
				if e.KeyPress() {
					if e.KeyString() != "ENTER" {
						if e.KeyString() == "L_SHIFT" {
							upper = true
						} else if e.KeyString() == "SPACE" {
							ch[i] = byte(' ')
							i++
						} else {
							if !upper {
								ch[i] = []byte(strings.ToLower(e.KeyString()))[0]
							} else {
								if !IsLetter(e.KeyString()) {
									ch[i] = byte(SymbolToUpper(rune(e.KeyString()[0])))
								} else {
									ch[i] = []byte(e.KeyString())[0]
								}
							}
							i++
						}
					} else {
						upper = false
						out <- ch[:i]
						i = 0
					}
				}

				if e.KeyRelease() {
					if e.KeyString() == "L_SHIFT" {
						upper = false
					}
				}
				break
			}
		}
	}
	//close(events)
	//k.Close()
	log.Println("Input Capture Terminated")
}

type KeyboardQRScanner struct {
	ignoreNewScan bool
	Address       string
	isListening   bool
	cancel        chan struct{}
	latestString  string
	isNew         bool
	lock          sync.Mutex
}

func (sb KeyboardQRScanner) Name() string {
	return sb.Address
}

func (sb *KeyboardQRScanner) IgnoreScan(enable bool) {
	sb.ignoreNewScan = enable
}

//WaitQR will fire callback when qr detected, new scan data will be discarded until cb is done
func (sb *KeyboardQRScanner) WaitQR(cb func(qr string)) {
	for {
		strQR := sb.ReadData()
		if strQR != "" {
			sb.ignoreNewScan = true
			cb(strQR)
			sb.ignoreNewScan = false
		}
		time.Sleep(500 * time.Millisecond)
	}
}

func (sb *KeyboardQRScanner) ReadData() string {
	retString := ""
	sb.lock.Lock()
	if sb.isNew {
		sb.isNew = false
		retString = strings.TrimSpace(sb.latestString)
	}
	sb.lock.Unlock()
	return retString
}

//StopListening will stop current goroutine to check available data on
func (s *KeyboardQRScanner) StopListening() {
	if s.isListening {
		close(s.cancel)
	}
}

func (s *KeyboardQRScanner) StartListening() {
	if !s.isListening {
		log.Println("Starting listening")
		s.cancel = make(chan struct{})
		s.isListening = true
		outChan := make(chan []byte)
		go s.qrCodeReaderWait(s.Address, outChan)
		go func() {
		Loop:
			for {
				select {
				case strNew := <-outChan:
					if !s.ignoreNewScan {
						s.lock.Lock()
						s.latestString = string(strNew)
						s.isNew = true
						s.lock.Unlock()
					}
				case <-s.cancel:
					log.Println("Barcode Listen loop stopped")
					s.lock.Lock()
					s.isNew = false
					s.latestString = ""
					s.isListening = false
					s.lock.Unlock()
					break Loop
				}
			}
		}()
	} else {
		log.Println("Already listening")
	}
}
