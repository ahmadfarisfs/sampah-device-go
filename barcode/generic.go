package barcode

import (
	"bufio"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/tarm/serial"
)

type BarcodeReaderFamily interface {
	//ReadData return empty string if nothing
	ReadData() string
	//WaitData(timeoutS int, cancel chan struct{}) string
	Name() string
	StartListening()
	WaitQR(func(string))
	StopListening()
	IgnoreScan(bool)
}

type VirtualBarcodeReader struct {
	Data       string
	enabled    bool
	DelayTimeS uint
	Created    time.Time
}

func (v VirtualBarcodeReader) IgnoreScan(yes bool) {

}

func (v VirtualBarcodeReader) Name() string {
	return "VIRTUAL"
}

func (v VirtualBarcodeReader) WaitQR(cb func(string)) {}

func (v VirtualBarcodeReader) WaitData(timeoutS int) string {
	time.Sleep(time.Second * time.Duration(v.DelayTimeS))
	if v.enabled {
		return "ASI"
	}
	return ""
}

func (v VirtualBarcodeReader) ReadData() string {

	return v.Data
}
func (v *VirtualBarcodeReader) StartListening() { v.enabled = true }
func (v *VirtualBarcodeReader) StopListening()  { v.enabled = false }

type SerialBarcodeReader struct {
	address      string
	device       *serial.Port
	isListening  bool
	latestString string
	isNew        bool
	lock         sync.Mutex
	cancel       chan struct{}
}

func CreateSerialBarcode(address string, baud int) (SerialBarcodeReader, error) {
	device := SerialBarcodeReader{address: address}
	config := &serial.Config{
		Name:        address,
		Baud:        baud,
		Parity:      serial.ParityNone,
		ReadTimeout: time.Duration(-1) * time.Second,
		StopBits:    serial.Stop1,
	}
	port, err := serial.OpenPort(config)
	if err != nil {
		log.Fatalf("Serial open failed: %v", err)
		return device, err
	}
	device.device = port
	device.isListening = false
	port.Flush()
	return device, nil

}
func (sb SerialBarcodeReader) contains(s string, e rune) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func (sb SerialBarcodeReader) Name() string {
	return sb.address
}

func (sb *SerialBarcodeReader) ReadData() string {
	retString := ""

	sb.lock.Lock()
	if sb.isNew {
		sb.isNew = false
		retString = sb.latestString
	}
	sb.lock.Unlock()

	return retString

}

func (sb SerialBarcodeReader) waitData() string {
	reader := bufio.NewReader(sb.device)

	//	s := make(chan string)
	//	e := make(chan error)
	bytesRead := []byte{}
	go func() {
		//	bytesRead := []byte{}
	Loop:
		for {
			toRead := reader.Buffered()
			if toRead >= 1 {
				_, err := reader.Read(bytesRead)
				if err != nil {
					log.Println("Reader Error: " + err.Error())
				}
				if sb.contains(string(bytesRead), rune('\n')) {
					break Loop
				}
			}
			select {
			case <-time.After(time.Duration(50) * time.Millisecond):
			case <-sb.cancel:
				//empty slice
				break Loop
			}
		}
	}()
	return strings.TrimSpace(string(bytesRead))
	/*	r, err := reader.ReadString('\n')
			if err != nil {
				e <- err
			} else {
				s <- r
			}8992761122317
			8992761122317

			close(e)
			close(s)
		}()
		if timeoutS < 0 {
			select {
			case rd := <-s:
				return strings.TrimSpace(rd)
			case errr := <-e:
				log.Println("Wait Barcode Error: ", errr.Error())
				return ""
			}
		}
		select {
		case rd := <-s:
			return strings.TrimSpace(rd)
		case errrr := <-e:
			log.Println("Wait Barcode Error: ", errrr.Error())
			return ""
		case <-time.After(time.Duration(timeoutS) * time.Second):
			log.Println("Wait Barcode timeout")
			return ""
		}*/
}

func (s *SerialBarcodeReader) StartListening() {

	s.device.Flush()
	if !s.isListening {
		log.Println("Start Listening")
		s.cancel = make(chan struct{})
		s.isListening = true
		go func() {
		Loop:
			for {
				strNew := s.waitData()
				s.lock.Lock()
				s.latestString = strNew
				s.isNew = true
				s.lock.Unlock()
				select {
				default:
				case <-s.cancel:
					log.Println("Barcode Listen loop stopped")
					s.isListening = false
					break Loop
				}
			}
		}()
	}
}

func (s *SerialBarcodeReader) StopListening() {
	if s.isListening {
		close(s.cancel)
	}
}
