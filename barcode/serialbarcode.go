package barcode

import (
	"bufio"
	"log"
	"strings"
	"time"

	"github.com/tarm/serial"
)

type SerialBarcode struct {
	port *serial.Port
}

func NewBarcode(address string, baud int) (SerialBarcode, error) {
	device := SerialBarcode{
		port: nil,
		//serialTimeoutSecond: timeoutS,
	}
	config := &serial.Config{
		Name:   address,
		Baud:   baud,
		Parity: serial.ParityNone,
		//ReadTimeout: time.Duration(timeoutS) * time.Second,
		StopBits: serial.Stop1,
	}
	port, err := serial.OpenPort(config)
	if err != nil {
		log.Fatalf("Serial open failed: %v", err)
		return device, err
	}
	device.port = port
	time.Sleep(time.Duration(2) * time.Second)
	port.Flush()
	return device, nil
}

func (sb SerialBarcode) StartListening(barcode chan string) {
	for {
		log.Println("Start waiting for barcode")
		strResponse, err := sb.waitResponse()
		log.Println("Barcode found")

		if err != nil {
			log.Println(err)
		}
		barcode <- strings.TrimSpace(strResponse)
	}
}

func (sb SerialBarcode) waitResponse() (string, error) {
	var err error
	reader := bufio.NewReader(sb.port)

	s := make(chan string)
	e := make(chan error)

	go func() {
		r, err := reader.ReadString('\n')
		if err != nil {
			e <- err
		} else {
			s <- r
		}
		close(e)
		close(s)
	}()

	select {
	case rd := <-s:
		return rd, err
	case err := <-e:
		return "", err
	}
}
