package validator

import (
	"fmt"
	"log"
	"os/exec"

	vision "cloud.google.com/go/vision/apiv1"
	"google.golang.org/api/option"

	//"github.com/ahmadfarisfs/gorecord"
	"context"
	//	"fmt"
	//	"log"
	"errors"
	"os"
)

type GVision struct {
	JSONLocation string
}

func (v GVision) CaptureImage() {
	//fswebcam -r 640x480 --jpeg 85 -D 1 temp.jpg
	outs := exec.Command("/bin/sh", "capture.sh")

	//time.Sleep(2 * time.Second)

	err := outs.Start()
	if err != nil {
		fmt.Printf("%s", err)
	}
	fmt.Println("Waiting")
	err = outs.Wait()
	fmt.Println("Waiting Done")

}

func (v GVision) Recognize(filename string) (map[string]float32, error) {
	v.CaptureImage()
	ctx := context.Background()

	// Creates a client.
	client, err := vision.NewImageAnnotatorClient(ctx, option.WithCredentialsFile(v.JSONLocation))
	if err != nil {
		return nil, errors.New("Failed to create client: " + err.Error())
	}
	defer client.Close()

	// Sets the name of the image file to annotate.
	//filename := "/tmp/temp.jpg"

	file, err := os.Open(filename)
	if err != nil {
		//		log.Fatalf()
		return nil, errors.New("Failed to read file:" + err.Error())
	}
	defer file.Close()
	image, err := vision.NewImageFromReader(file)
	if err != nil {
		//		log.Fatalf("Failed to create image: %v", err)
		return nil, errors.New("Failed to create image: " + err.Error())
	}

	labels, err := client.DetectLabels(ctx, image, nil, 50)
	if err != nil {
		//		log.Fatalf()
		return nil, errors.New("Failed to detect labels: " + err.Error())
	}
	pred := map[string]float32{}
	//fmt.Println("Labels:")
	log.Println("AI RESULT_____________________ START")

	for _, label := range labels {
		pred[label.Description] = label.Topicality
		log.Println(label.Description + " " + fmt.Sprintf("%f", label.Topicality) + " " + fmt.Sprintf("%f", label.Confidence))

	}
	log.Println("AI RESULT_____________________ END")

	return pred, nil
}

func (v *GVision) StartCamera() error {
	//	return //v.camera.Open()
	return nil
}

func (v GVision) StopCamera() error {
	//	return v.camera.Close()
	return nil
}

// localizeObjects gets objects and bounding boxes from the Vision API for an image at the given file path.
func (v GVision) LocalizeObjects(file string) error {
	ctx := context.Background()

	client, err := vision.NewImageAnnotatorClient(ctx, option.WithCredentialsFile(v.JSONLocation))
	if err != nil {
		return err
	}
	log.Println("fsafs")
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	image, err := vision.NewImageFromReader(f)
	if err != nil {
		return err
	}
	annotations, err := client.LocalizeObjects(ctx, image, nil)
	if err != nil {
		return err
	}

	if len(annotations) == 0 {
		log.Println("No objects found.")
		return nil
	}

	log.Println("Objects:")
	for _, annotation := range annotations {
		log.Println(annotation.Name)
		log.Println(annotation.Score)

		for _, v := range annotation.BoundingPoly.NormalizedVertices {
			log.Println("(%f,%f)\n", v.X, v.Y)
		}
	}

	return nil
}
