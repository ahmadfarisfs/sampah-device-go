package validator

import "strings"

const (
	UnknownMaterial = iota
	GlassMaterial
	PlasticMaterial
)

type MaterialInfo int

func Validate(pred map[string]float32) (bool, MaterialInfo) {
	isBottle := false
	//materials := UnknownMaterial
	var materials MaterialInfo
	materials = UnknownMaterial
	for key, _ := range pred {
		if strings.Contains(strings.ToLower(key), "bottle") && isBottle == false {
			isBottle = true
		}
		if strings.Contains(strings.ToLower(key), "plastic") && materials == UnknownMaterial {
			materials = PlasticMaterial
		}
		if strings.Contains(strings.ToLower(key), "glass") && materials == UnknownMaterial {
			materials = GlassMaterial
		}
	}

	return isBottle, materials
}
