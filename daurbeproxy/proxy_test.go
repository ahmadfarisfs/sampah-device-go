package daurbeproxy

import (
	"bytes"
	"net"
	"os"
	"testing"
)

func getMacAddr() (addr string) {
	interfaces, err := net.Interfaces()
	if err == nil {
		for _, i := range interfaces {
			if i.Flags&net.FlagUp != 0 && bytes.Compare(i.HardwareAddr, nil) != 0 {
				// Don't use random as we have a real address
				addr = i.HardwareAddr.String()
				break
			}
		}
	}
	return
}
func TestRegisterBox(t *testing.T) {
	proxy := CreateDaurProxy("dev-api.omitsindo.com", 3)
	hostname, _ := os.Hostname()
	//err := proxy.RegisterBox(hostname, getMacAddr())
	//if err != nil {
	//	t.Error(err)
	//	return
	//}
	token, err := proxy.LoginBox(hostname+"1", getMacAddr())
	if err != nil {
		t.Error(err)
		return
	}
	t.Log("Token: " + token)
	_, errAuth := proxy.Authenticate()
	if errAuth != nil {
		t.Error(errAuth)
		return
	}

	_, sessionID, userID, err := proxy.CheckUser("CKRaHQlt10g1YjZjN2I1My1mYjc1LTRjOTgtYWI5OC0yYTQzOGUzZTQ1ZTk=")
	if err != nil {
		t.Error(err)
		return

	}
	trashList := map[string]uint64{
		"OUFHAO":  90,
		"ofahsof": 9,
	}
	errs := proxy.TrashInputReport(trashList, userID, sessionID)
	if errs != nil {
		t.Error(errs)
		return

	}
	t.Log("Success")
}
