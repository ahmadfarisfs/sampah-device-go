package daurbeproxy

import (
	//daur "sampah-device-go/error"
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type UserRole int

const (
	DaurUndefined UserRole = -1
	DaurUser      UserRole = 0
	DaurAdmin     UserRole = 1
)

type DaurProxyFamily interface {
	RegisterBox(mac string, hostname string) error        //First thing to do, box should be registered
	LoginBox(mac string, hostname string) (string, error) //Every login generate new token
	Authenticate() (bool, error)                          //Check token validity

	CheckUser(qrCode string) (role UserRole, sessionID string, userID string, status error) //After scan, check using this, so we get sessionID
	CheckSKU(sku string) (bool, error)                                                      //Used to check wether SKU is valid or not
	ReportBox(binPercentage float32, upSince time.Time) error                               //Heartbeat report
	FailureBoxReport(reason string) error

	TrashInputReport(trash map[string]uint64, userID string, sessionID string) error //Report after user has finished entering trash
	EndSession(sessionID string) error                                               //End user session
	BanUser(userID string, reason string) error                                      //Ban User
}

//secret palsu
//isZ5/yNp10g5YjlhYmVhZi1hMGU0LTQ5NzctOWI3Zi04ZmNjMGZlNTEwMmU=

//token
//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIzZGMwZjVjZi02YzZkLTQxNTctOTRhYi1lZGU1OTkyODY5NmYiLCJzdWIiOiJIUEdhbWluZzEiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJib3giLCJleHAiOjE3MzEzNDQzOTksImlzcyI6IkRhdXIiLCJhdWQiOiJodHRwczovL29taXRzaW5kby5jb20vZGF1ciJ9.J8pqRr9jKxO95j5Ep-CsS6rhy1pRuS7PXO1YhVUP8rM
type DaurProxy struct {
	hostname string
	timeoutS int

	token         string
	serverAddress string
	client        *http.Client
}

func CreateDaurProxy(serverAddress string, timeoutS int) DaurProxy {
	return DaurProxy{
		serverAddress: serverAddress,
		client:        &http.Client{Timeout: time.Second * time.Duration(timeoutS)},
	}
}
func (d DaurProxy) ReportBox(binPercentage float32, upSince time.Time) error { return nil }

func (d DaurProxy) FailureBoxReport(reason string) error { return nil }

func (d DaurProxy) cleanInputString(input string) string {
	reg, _ := regexp.Compile("[^a-zA-Z0-9]+")

	return reg.ReplaceAllString(strings.TrimSpace(input), "")
}

func (d DaurProxy) RegisterBox(mac string, hostname string) error {
	payload := map[string]string{
		"hostname": d.cleanInputString(hostname),
		"mac":      d.cleanInputString(mac),
	}
	payloadBytes, errM := json.Marshal(payload)
	if errM != nil {
		return errors.New("Error Marshaling RegisterBox: " + errM.Error())
	}
	respBytes, err := d.sendRequestWithAuth("https://"+d.serverAddress+"/daur/api/box/register", 200, payloadBytes, false)
	if err != nil {
		return errors.New("Error RegisterBox: " + err.Error())
	}

	respStruct := struct {
		IsSuccess    bool   `json:"isSuccess"`
		ErrorMessage string `json:"errorMessage"`
	}{}

	errUn := json.Unmarshal(respBytes, &respStruct)
	if errUn != nil {
		return errors.New("Error Unmarshal response RegisterBox: " + errUn.Error())
	}

	log.Println("----------------------------------->")
	log.Println("BODY RESP:")
	log.Println(string(respBytes))
	log.Println(respStruct.IsSuccess)
	log.Println(respStruct.ErrorMessage)
	log.Println("----------------------------------->")

	if !respStruct.IsSuccess {
		return errors.New("Fail to RegisterBox because " + respStruct.ErrorMessage)
	}
	return nil
}

//First thing to do, box should be registered
func (d *DaurProxy) LoginBox(mac string, hostname string) (string, error) {
	payload := map[string]string{
		"boxId": d.cleanInputString(mac) + "::" + d.cleanInputString(hostname),
	}
	payloadBytes, errM := json.Marshal(payload)
	if errM != nil {
		return "", errors.New("Error Marshaling LoginBox: " + errM.Error())
	}
	//	log.Println(string(payloadBytes))

	respBytes, err := d.sendRequestWithAuth("https://"+d.serverAddress+"/daur/api/box/login", 200, payloadBytes, false)
	if err != nil {
		return "", errors.New("Error LoginBox: " + err.Error())
	}
	respStruct := struct {
		IsSuccess bool `json:"isSuccess"`
		//	Value     struct {
		//		Token string `json:"token"`
		//	} `json:"value"`
	}{}
	errUn := json.Unmarshal(respBytes, &respStruct)
	if errUn != nil {
		return "", errors.New("Error Unmarshal response LoginBox: " + errUn.Error())
	}
	if !respStruct.IsSuccess {
		respStructNeg := struct {
			IsSuccess    bool   `json:"isSuccess"`
			ErrorMessage string `json:"errorMessage"`
		}{}
		errUnpackNeg := json.Unmarshal(respBytes, &respStructNeg)
		if errUnpackNeg != nil {
			return "", errors.New("Error Unmarshal negative response " + errUnpackNeg.Error())
		}
		return "", errors.New("Error Login because of " + respStructNeg.ErrorMessage)
	}
	respStructPos := struct {
		IsSuccess bool `json:"isSuccess"`
		Value     struct {
			Token string `json:"token"`
		} `json:"value"`
	}{}
	errUnpackPos := json.Unmarshal(respBytes, &respStructPos)
	if errUnpackPos != nil {
		return "", errors.New("Error Unmarshal positive response " + errUnpackPos.Error())
	}
	d.token = respStructPos.Value.Token
	log.Println("Login token: " + d.token)
	return d.token, nil
}

//Every login generate new token
func (d DaurProxy) Authenticate() (bool, error) {
	respBytes, err := d.sendRequestWithAuth("https://"+d.serverAddress+"/daur/api/box/authenticate", 200, nil, true)
	if err != nil {
		return false, errors.New("Error Authenticate: " + err.Error())
	}
	respStructPos := struct {
		IsSuccess    bool   `json:"isSuccess"`
		ErrorMessage string `json:"errorMessage,omitempty"`
		Value        struct {
			Token string `json:"token,omitempty"`
		} `json:"value,omitempty"`
	}{}
	errUnpackPos := json.Unmarshal(respBytes, &respStructPos)
	if errUnpackPos != nil {
		return false, errors.New("Error Unmarshal Authenticate response " + errUnpackPos.Error())
	}
	if !respStructPos.IsSuccess {
		return false, errors.New("Authentication failed")
	}
	return true, nil
} //Check token validity

//CheckUser checks user after scan, so we get sessionID
func (d DaurProxy) CheckUser(qrCode string) (role UserRole, sessionID string, userID string, status error) {
	reqPayload := map[string]string{
		"secret": qrCode,
	}
	payloadBytes, errMarshal := json.Marshal(reqPayload)
	if errMarshal != nil {
		return DaurUndefined, "", "", errors.New("Error Marshaling CheckUser: " + errMarshal.Error())
	}
	log.Println(string(payloadBytes))
	respBytes, err := d.sendRequestWithAuth("https://"+d.serverAddress+"/daur/api/box/validate/user", 200, payloadBytes, true)
	if err != nil {
		return DaurUndefined, "", "", errors.New("Error Check User: " + err.Error())
	}
	respStructPos := struct {
		IsSuccess    bool   `json:"isSuccess"`
		ErrorMessage string `json:"errorMessage,omitempty"`
		Value        struct {
			Token     string `json:"token,omitempty"`
			UserID    string `json:"userId,omitempty"`
			Bottles   int    `json:"bottles,omitempty"`
			Point     int    `json:"point,omitempty"`
			Name      string `json:"name,omitempty"`
			Role      string `json:"role,omitempty"`
			SessionID string `json:"sessionId,omitempty"`
		} `json:"value,omitempty"`
	}{}
	errUnpackPos := json.Unmarshal(respBytes, &respStructPos)
	if errUnpackPos != nil {
		return DaurUndefined, "", "", errors.New("Error Unmarshal CheckUser " + errUnpackPos.Error())
	}
	if !respStructPos.IsSuccess {
		return DaurUndefined, "", "", errors.New("Check User Not success: " + respStructPos.ErrorMessage)
	}
	//parse role

	return DaurUser, respStructPos.Value.SessionID, respStructPos.Value.UserID, nil
}

//CheckSKU will check validity of SKU on the server
func (d DaurProxy) CheckSKU(sku string) (bool, error) {
	return false, nil
}

//Heartbeat report
func (d DaurProxy) Report(binPercentage float32, upSince time.Time) error {
	return nil
}

func (d DaurProxy) TrashInputReport(trash map[string]uint64, userID string, sessionID string) error {

	reqStruct := struct {
		UserID    string `json:"userId"`
		SessionID string `json:"sessionId"`
		SKU       []struct {
			Code  string `json:"code"`
			Total uint64 `json:"total"`
		} `json:"sku"`
	}{}
	reqStruct.UserID = userID
	reqStruct.SessionID = sessionID
	for key, val := range trash {
		reqStruct.SKU = append(reqStruct.SKU, struct {
			Code  string `json:"code"`
			Total uint64 `json:"total"`
		}{
			Code:  key,
			Total: val,
		})
	}
	payloadBytes, errMarshal := json.Marshal(reqStruct)
	log.Println(string(payloadBytes))
	if errMarshal != nil {
		return errors.New("Error Marshaling TrashReport: " + errMarshal.Error())
	}
	respBytes, err := d.sendRequestWithAuth("https://"+d.serverAddress+"/daur/api/box/trash", 200, payloadBytes, true)
	if err != nil {
		return errors.New("Error TrashReport: " + err.Error())
	}
	respStructPos := struct {
		IsSuccess    bool   `json:"isSuccess"`
		ErrorMessage string `json:"errorMessage,omitempty"`
	}{}
	log.Println(string(respBytes))
	errUnpackPos := json.Unmarshal(respBytes, &respStructPos)
	if errUnpackPos != nil {
		return errors.New("Error Unmarshal TrashReport " + errUnpackPos.Error())
	}
	if !respStructPos.IsSuccess {
		return errors.New("TrashReport Not success: " + respStructPos.ErrorMessage)
	}

	return nil
}

//Report after user has finished entering trash
func (d DaurProxy) EndSession(sessionID string) error {
	reqPayload := map[string]string{
		"sessionId": sessionID,
	}
	payloadBytes, errMarshal := json.Marshal(reqPayload)
	if errMarshal != nil {
		return errors.New("Error Marshaling CheckUser: " + errMarshal.Error())
	}
	respBytes, err := d.sendRequestWithAuth("https://"+d.serverAddress+"/daur/api/box/user/logout", 200, payloadBytes, true)
	if err != nil {
		return errors.New("Error Check User: " + err.Error())
	}
	respStructPos := struct {
		IsSuccess    bool   `json:"isSuccess"`
		ErrorMessage string `json:"errorMessage,omitempty"`
	}{}
	errUnpackPos := json.Unmarshal(respBytes, &respStructPos)
	if errUnpackPos != nil {
		return errors.New("Error Unmarshal Logout " + errUnpackPos.Error())
	}
	if !respStructPos.IsSuccess {
		return errors.New("Logout Not success: " + respStructPos.ErrorMessage)
	}

	return nil
}

//End user session
func (d DaurProxy) BanUser(userID string, reasong string) error {
	return nil
} //Ban User

func (t DaurProxy) sendRequestWithAuth(url string, expStatusCode int, bodyPayload []byte, useAuth bool) (responseBody []byte, err error) {
	var reqAction string
	if bodyPayload == nil {
		reqAction = "GET"
	} else {
		reqAction = "POST"
	}

	req, err := http.NewRequest(reqAction, url, bytes.NewBuffer(bodyPayload))
	if useAuth {
		bearer := "Bearer " + t.token
		req.Header.Add("Authorization", bearer)

		//	log.Println("HTTP " + reqAction + " to address: " + url + "\r\nWith token: " + t.token + "\r\nWith payload: " + string(bodyPayload))
	}

	if reqAction == "POST" {
		req.Header.Add("Content-Type", "application/json")
	}

	resp, err := t.client.Do(req)
	if err != nil {
		errMsg := "Error doing " + reqAction + "request: " + err.Error()
		log.Println(errMsg)
		return nil, errors.New(errMsg)
	}
	defer resp.Body.Close()

	//	log.Println("Resp Status Code: " + strconv.Itoa(resp.StatusCode))
	if resp.StatusCode != expStatusCode {
		return nil, errors.New("Response code does not match expectation, expect " + strconv.Itoa(expStatusCode) + " got " + strconv.Itoa(resp.StatusCode) + " to url: " + url)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln("Cannot Read Reponse: " + err.Error())
		return nil, err
	}
	return body, nil
}
