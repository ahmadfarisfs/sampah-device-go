package sampahduino

import (
	"bufio"
	"sync"

	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/tarm/serial"
)

type SampahDeviceFamily interface {
	//CreateSampahDevice(devicepath string, baud, int, timeOutS uint64) (SampahDeviceFamily, error)
	Ping() error

	StartReaderRotator() error
	StopReaderRotator() error
	SetReaderRotatorConfig(delay uint8, startPos uint8, endPos uint8)

	GetUltrasonic(id int) (int, error)
	GetTof(id int) (int, error)
	GetBool(id int) (bool, error)

	SetPWM(id int, value uint) error
	SetServo(id int, angle uint) error

	SetStepper(angle int) error
}

type SampahDevice struct {
	port                *serial.Port
	serialTimeoutSecond uint64
	mux                 sync.Mutex
	barcodeRotatorSpeed uint8 //default 25
	barcodeStartPos     uint8 //save limit 60
	barcodeEndPos       uint8 //save limit 110
}

func (d SampahDevice) SetStepper(angle int) error {
	//Hack way, implementation on duino should be synchronous
	d.writeAndWait("P:" + strconv.Itoa(int(angle)) + "\r\n")
	return nil
}
func CreateSampahDevice(devicepath string, baud int, timeoutS uint64) (SampahDeviceFamily, error) {
	device := SampahDevice{
		port:                nil,
		serialTimeoutSecond: timeoutS,
		barcodeRotatorSpeed: 25,
		barcodeStartPos:     60,
		barcodeEndPos:       110,
	}
	config := &serial.Config{
		Name:        devicepath,
		Baud:        baud,
		Parity:      serial.ParityNone,
		ReadTimeout: time.Duration(timeoutS) * time.Second,
		StopBits:    serial.Stop1,
	}
	port, err := serial.OpenPort(config)
	if err != nil {
		log.Fatalf("Serial open failed: %v", err)
	} else {
		device.port = port
	}
	time.Sleep(time.Duration(5) * time.Second)
	port.Flush()
	//log.Println("Sampah device created")
	/*log.Println("Waiting Device to be ready")
	err = device.waitReady()
	log.Println("Done Waiting ")

	if err != nil {
		log.Println(err)

		return device, err
	}
	*/
	return device, err
}

func (d SampahDevice) waitReady() error {
	_, err := d.waitResponse()
	if err != nil {
		return err
	}
	d.port.Flush()
	return nil
}

//StartReaderRotator will asynchronously start reader rotator motor
func (d SampahDevice) StartReaderRotator() error {
	_, err := d.writeAndWait("O:1:" + strconv.Itoa(int(d.barcodeStartPos)) + ":" + strconv.Itoa(int(d.barcodeEndPos)) + ":" + strconv.Itoa(int(d.barcodeRotatorSpeed)) + "\r\n")
	if err != nil {
		return errors.Wrap(err, "Error starting reader rotator ")
	}
	_, err = d.writeAndWait("R:3:255\r\n")
	if err != nil {
		return errors.Wrap(err, "Error turning on ")
	}

	return nil
}

//StopReaderRotator will asynchronously stop reader rotator motor
func (d SampahDevice) StopReaderRotator() error {
	_, err := d.writeAndWait("O:0\r\n") // + strconv.Itoa(int(d.barcodeStartPos)) + ":" + strconv.Itoa(int(d.barcodeStopPos)) + ":" + strconv.Itoa(int(d.barcodeRotatorSpeed)) + "\r\n")
	if err != nil {
		return errors.Wrap(err, "Error stoping reader rotator ")
	}
	_, err = d.writeAndWait("R:3:0\r\n")
	if err != nil {
		return errors.Wrap(err, "Error turning off ")
	}

	return nil
}

//SetReaderRotatorConfig set the config for reader rotator
func (d SampahDevice) SetReaderRotatorConfig(speed uint8, startPos uint8, endPos uint8) {
	d.barcodeRotatorSpeed = speed
	d.barcodeStartPos = startPos
	d.barcodeEndPos = endPos
}

func (d SampahDevice) writeString(data string) error {
	dataBytes := []byte(data)
	_, err := d.port.Write(dataBytes)
	if err != nil {
		log.Println("Error write to serial")
		return err
	}
	return nil
}

func (d SampahDevice) waitResponse() (string, error) {
	var err error
	reader := bufio.NewReader(d.port)

	s := make(chan string)
	e := make(chan error)

	go func() {
		r, err := reader.ReadString('\n')
		if err != nil {
			e <- err
		} else {
			s <- r
		}
		close(e)
		close(s)
	}()

	select {
	case rd := <-s:
		return rd, err
	case err := <-e:
		return "", err
	case <-time.After(time.Duration(d.serialTimeoutSecond) * time.Second):
		return "", errors.New("Wait Response Timeout")
	}
}

func (d SampahDevice) SetServo(id int, angle uint) error {

	if angle < 0 || angle > 180 {
		return fmt.Errorf("Angle out of range id: %d angle %d", id, angle)
	}
	_, err := d.writeAndWait("S:" + strconv.Itoa(id) + ":" + strconv.Itoa(int(angle)) + "\r\n")
	return err
}

func (d SampahDevice) Ping() error {
	log.Println("Pinging device")
	_, err := d.writeAndWait("I\r\n")
	return errors.Wrap(err, "Ping error")
}

func (d SampahDevice) GetTof(id int) (int, error) {
	log.Println("Getting ToF id: ", id)
	value, err := d.writeWaitAndParse("F:" + strconv.Itoa(id) + "\r\n")
	return value, err
}

func (d SampahDevice) GetUltrasonic(id int) (int, error) {
	//log.Println("Getting uSonic id: ", id)
	value, err := d.writeWaitAndParse("U:" + strconv.Itoa(id) + "\r\n")
	return value, err
}

func (d SampahDevice) GetBool(id int) (bool, error) {
	log.Println("Getting boolean sensor id: ", id)

	value, err := d.writeWaitAndParse("B:" + strconv.Itoa(id) + "\r\n")
	if err != nil {
		return false, err
	}

	boolValue, errParseBool := strconv.ParseBool(strconv.Itoa(value))
	if errParseBool != nil {
		return false, errParseBool
	}
	return boolValue, err
}

func (d SampahDevice) writeWaitAndParse(command string) (value int, err error) {
	resp, err := d.writeAndWait(command)
	if err != nil {
		return 0, err
	}
	resp = strings.TrimSpace(resp)
	arrStr := strings.Split(resp, ":")

	if len(arrStr) != 3 {
		return 0, errors.New("Return Value Parsing Error")
	}
	value, parseError := strconv.Atoi(arrStr[2])

	if parseError != nil {
		return 0, errors.New("Value Parsing Error")
	}
	return value, nil

}

func (d SampahDevice) SetPWM(id int, value uint) error {

	_, err := d.writeAndWait("R:" + strconv.Itoa(id) + ":" + strconv.Itoa(int(value)) + "\r\n")
	return err
}

func (d *SampahDevice) writeAndWait(command string) (string, error) {
	d.mux.Lock()
	err := d.writeString(command)
	if err != nil {
		d.mux.Unlock()

		return "", err
	}
	strResponse, errResponse := d.waitResponse()
	if errResponse != nil {
		d.mux.Unlock()

		return "", errResponse
	}
	d.mux.Unlock()
	errProcess := d.processResponse(strResponse)
	if errProcess != nil {
		return "", errProcess
	}
	return strResponse, nil

}

func (d SampahDevice) processResponse(response string) error {
	if response[0] == 'T' {
		if response[2] == '1' {
			return nil
		} else if response[2] == '0' {
			return errors.New("Parser: Unrecognized Command")
		}
		return errors.New("Parser: wrong Command")
	}
	return fmt.Errorf("Parser: Packet header error %b", response[0])

}
