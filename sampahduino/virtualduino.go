package sampahduino

import "time"

type VirtualDuino struct {
	BoolStatus  []bool
	TofStatus   []int
	UltraStatus []int
	startTime   time.Time
}

func (v VirtualDuino) Ping() error {
	return nil
}

func (v VirtualDuino) StartReaderRotator() error {
	v.startTime = time.Now()
	return nil
}
func (v VirtualDuino) StopReaderRotator() error                                         { return nil }
func (v VirtualDuino) SetReaderRotatorConfig(speed uint8, startPos uint8, endPos uint8) {}

func (v VirtualDuino) GetUltrasonic(id int) (int, error) { return v.UltraStatus[id], nil }
func (v VirtualDuino) GetTof(id int) (int, error)        { return v.TofStatus[id], nil }
func (v VirtualDuino) GetBool(id int) (bool, error)      { return v.BoolStatus[id], nil }

func (v VirtualDuino) SetPWM(id int, value uint) error   { return nil }
func (v VirtualDuino) SetServo(id int, angle uint) error { return nil }
func (v VirtualDuino) SetStepper(angle int) error {
	return nil
}
